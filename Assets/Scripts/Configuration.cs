﻿namespace Assets.Scripts
{
    class Configuration
    {
        public const string 
            FTPHost = "",
            FTPUsername = "",
            FTPPassword = "",
            MeshPath = "",
            ModifiedName = "",
            TexturePath = "";
    }
}
