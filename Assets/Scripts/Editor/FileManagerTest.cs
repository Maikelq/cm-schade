﻿using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Rhino.Mocks;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using UnityEngine;

namespace Assets.Scripts.Editor
{
    public class FileManagerTest
    {
        private Mock<FileManager> _fileManager;

        [SetUp]
        public void SetUp()
        {
            // Setup
            _fileManager = new Mock<FileManager>();
        }

        [Test]
        public void OriginalTextureIsLoadedCorrectly()
        {
            // Arrange
            _fileManager.Setup(f => f.DownloadWithFTP(It.IsAny<string>())).Returns(System.Array.Empty<byte>());
            _fileManager.Setup(f => f.LoadTexture("", "", false)).CallBase();

            // Act
            _fileManager.Object.LoadTexture("", "", false);

            // Assert
            _fileManager.Verify(f => f.DownloadWithFTP(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void SaveToPngCallsForFTPSavingMethod()
        {
            // Arrange 
            var texture = new Texture2D(10, 10);
            _fileManager.Setup(f => f.SaveToPNG(texture, "", "")).CallBase();

            // Act
            _fileManager.Object.SaveToPNG(texture, "", "");

            // Assert
            _fileManager.Verify(f => f.SaveWithFTP(It.IsAny<string>(), It.IsAny<byte[]>()), Times.Once());
        }

        [Test]
        public void FileManagerReturnsModelVersionsCorrectly()
        {
            // Arrange 
            _fileManager.Setup(f => f.GetModelVersions("")).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            var ftpWebResponse = Rhino.Mocks.MockRepository.GenerateStub<FtpWebResponse>();
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            // Data in XML format (Windows)
            var response = "05-01-20  11:22AM            364067629 test1.xml\r\n" +
                "05-01-20  11:22AM            142453619 test2.xml";
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(response));
            _fileManager.Setup(f => f.GetWebResponse(It.IsAny<FtpWebRequest>())).Returns(ftpWebResponse);
            _fileManager.Setup(f => f.GetWebResponseStream(It.IsAny<FtpWebResponse>())).Returns(stream);
            
            // Act
            var result = _fileManager.Object.GetModelVersions("");

            // Assert
            Assert.IsTrue(result.Count == 2);
            Assert.AreEqual(result[0], "test1");
            Assert.AreEqual(result[1], "test2");        
        }

        [Test]
        public void FileManagerSuccessfullyRetrievesFileWithFtpWebRequestWhenCheckingForModifiedTextureFile()
        {
            // Arrange 
            _fileManager.Setup(f => f.IsModifiedTextureAvailable("", "")).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            var ftpWebResponse = Rhino.Mocks.MockRepository.GenerateStub<FtpWebResponse>();
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.GetWebResponse(It.IsAny<FtpWebRequest>())).Returns(ftpWebResponse);

            // Act
            var result = _fileManager.Object.IsModifiedTextureAvailable("", "");

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void FileManagerFailsToRetrieveFileWithFtpWebRequestWhenCheckingForModifiedTextureFile()
        {
            // Arrange 
            _fileManager.Setup(f => f.IsModifiedTextureAvailable("", "")).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            var ftpWebResponse = Rhino.Mocks.MockRepository.GenerateStub<FtpWebResponse>();
            ftpWebResponse.Stub(f => f.StatusCode).Return(FtpStatusCode.ActionNotTakenFileUnavailable);
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.GetWebResponse(It.IsAny<FtpWebRequest>())).Throws(new WebException("error", null, WebExceptionStatus.RequestCanceled, ftpWebResponse));

            // Act
            var result = _fileManager.Object.IsModifiedTextureAvailable("", "");

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void FileManagerSetsDamageAnalysisCompletionCorrectly()
        {
            // Arrange 
            _fileManager.Setup(f => f.SetDamageAnalysisCompletion("test", States.ModelStateEnum.Finished)).CallBase();
            _fileManager.Setup(f => f.SerializeObject(It.IsAny<Dictionary<string, int>>())).Returns((XmlDocument) null);

            // Act
            _fileManager.Object.SetDamageAnalysisCompletion("test", States.ModelStateEnum.Finished);

            // Assert
            Assert.IsTrue(_fileManager.Object._modelStates.ContainsKey("test"));
            _fileManager.Verify(s => s.GetWebRequestStream(It.IsAny<FtpWebRequest>()), Times.Exactly(0));
        }

        [Test]
        public void FileManagerSetsAndSavesDamageAnalysisCompletionCorrectly()
        {
            // Arrange 
            _fileManager.Setup(f => f.SetDamageAnalysisCompletion("test", States.ModelStateEnum.Finished)).CallBase();
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(""));
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.GetWebRequestStream(It.IsAny<FtpWebRequest>())).Returns(stream);
            _fileManager.Setup(f => f.SerializeObject(It.IsAny<Dictionary<string, int>>())).Returns(new XmlDocument());

            // Act
            _fileManager.Object.SetDamageAnalysisCompletion("test", States.ModelStateEnum.Finished);

            // Assert
            Assert.IsTrue(_fileManager.Object._modelStates.ContainsKey("test"));
            _fileManager.Verify(s => s.GetWebRequestStream(It.IsAny<FtpWebRequest>()), Times.Once());
        }

        [Test]
        public void FileManagerReturnsDamageAnalysisCompletionValueCorrectlyWithNewEntry()
        {
            // Arrange 
            _fileManager.Setup(f => f.IsDamageAnalysisCompleted("test")).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.DeserializeObject(It.IsAny<FtpWebRequest>()));

            // Act
            var result = _fileManager.Object.IsDamageAnalysisCompleted("test");

            // Assert
            Assert.IsTrue(result == 1);
        }

        [Test]
        public void FileManagerReturnsDamageAnalysisCompletionValueCorrectlyWithExistingEntry()
        {
            // Arrange 
            _fileManager.Object._modelStates.Add("test", 2);
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.IsDamageAnalysisCompleted("test")).CallBase();
            _fileManager.Setup(f => f.DeserializeObject(It.IsAny<FtpWebRequest>()));

            // Act
            var result = _fileManager.Object.IsDamageAnalysisCompleted("test");

            // Assert
            Assert.IsTrue(_fileManager.Object._modelStates.ContainsKey("test"));
            Assert.IsTrue(result == 2);
        }

        [Test]
        public void FileManagerDownloadsByteArrayWithFTP()
        {
            // Arrange 
            _fileManager.Setup(f => f.DownloadWithFTP("")).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            var ftpWebResponse = Rhino.Mocks.MockRepository.GenerateStub<FtpWebResponse>();
            var response = "test";
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(response));
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.GetWebResponse(It.IsAny<FtpWebRequest>())).Returns(ftpWebResponse);
            _fileManager.Setup(f => f.GetWebResponseStream(It.IsAny<FtpWebResponse>())).Returns(stream);

            // Act
            var result = _fileManager.Object.DownloadWithFTP("");

            // Assert
            Assert.AreEqual(Encoding.UTF8.GetString(result), response);
        }

        [Test]
        public void FileManagerSavesByteArrayWithFTP()
        {
            // Arrange 
            var bytes = new byte[] { };
            _fileManager.Setup(f => f.SaveWithFTP("", bytes)).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(""));
            _fileManager.Setup(f => f.CreateWebRequest(It.IsAny<string>())).Returns(ftpWebRequest);
            _fileManager.Setup(f => f.GetWebRequestStream(It.IsAny<FtpWebRequest>())).Returns(stream);

            // Act
            _fileManager.Object.SaveWithFTP("", bytes);

            // Assert
            _fileManager.Verify(s => s.GetWebRequestStream(It.IsAny<FtpWebRequest>()), Times.Once());
        }

        [Test]
        public void FileManagerSerializesAnObjectCorrectly()
        {
            // Arrange 
            var document = new XmlDocument();
            _fileManager.Setup(f => f.SerializeObject(It.IsAny<Dictionary<string, int>>())).CallBase();
            _fileManager.Setup(f => f.SerializeObject(It.IsAny<Dictionary<string, int>>())).Returns(document);

            // Act
            var result = _fileManager.Object.SerializeObject(new Dictionary<string, int>());

            // Assert
            Assert.AreEqual(result, document);
        }

        [Test]
        public void FileManagerDeserializesAnObjectCorrectly()
        {
            // Arrange 
            _fileManager.Setup(f => f.DeserializeObject(It.IsAny<FtpWebRequest>())).CallBase();
            var ftpWebRequest = Rhino.Mocks.MockRepository.GenerateStub<FtpWebRequest>();
            var ftpWebResponse = Rhino.Mocks.MockRepository.GenerateStub<FtpWebResponse>();
            // XML format state of models
            var response = "<items><Item Id = \"Test\" Value = \"1\" /></items>";
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(response));
            _fileManager.Setup(f => f.GetWebResponse(It.IsAny<FtpWebRequest>())).Returns(ftpWebResponse);
            _fileManager.Setup(f => f.GetWebResponseStream(It.IsAny<FtpWebResponse>())).Returns(stream);

            // Act
            _fileManager.Object.DeserializeObject(ftpWebRequest);

            // Assert
            Assert.IsTrue(_fileManager.Object._modelStates.Count == 1);
            Assert.IsTrue(_fileManager.Object._modelStates["Test"] == 1);
        }
    }
}