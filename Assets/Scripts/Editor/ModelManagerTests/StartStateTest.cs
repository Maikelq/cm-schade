﻿using Assets.Scripts.States;
using Lean.Touch;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Editor.ModelManagerTests
{
    public class StartStateTest
    {

        private ModelManager _modelManager;
        private StartState _startState;

       [SetUp]
        public void SetUp()
        {
            // Setup managers
            _modelManager = new ModelManager();
            _startState = new StartState(_modelManager);
            _modelManager.ModelState = _startState;
        }

        [Test]
        public void StartStateResetsPositionCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();

            // Act
            _startState.ResetPos();

            // Assert
            Assert.AreEqual(_modelManager.ArModel.transform.localScale, new Vector3(0.45f, 0.45f, 0.45f));
        }

        [Test]
        public void StartStateResetsPositionCorrectlyWithExceptions()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<Transform>();
            _modelManager.ArModel.transform.name = "Baron";

            // Act
            _startState.ResetPos();

            // Assert
            Assert.AreEqual(_modelManager.ArModel.transform.localScale, new Vector3(0.2f, 0.2f, 0.2f));
        }

        [Test]
        public void StartStateTogglesMarkingCorrectly()
        {
            // Act
            _startState.ToggleMarking();

            // Assert
            Assert.IsFalse(_modelManager.MarkingEnabled);
        }

        [Test]
        public void StartStateTogglesScaleScriptCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanPinchScale>();

            // Act
            _startState.ToggleScaleScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanPinchScale>().enabled);
        }

        [Test]
        public void StartStateTogglesTwistRotateScriptOffIfScaleScriptIsActive()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanPinchScale>().enabled = false;
            _modelManager.ArModel.AddComponent<LeanTwistRotate>().enabled = true;

            // Act
            _startState.ToggleScaleScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanTwistRotate>().enabled);
        }

        [Test]
        public void StartStateTogglesTwistRotateScriptCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanTwistRotate>();

            // Act
            _startState.ToggleRotateScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanTwistRotate>().enabled);
        }

        [Test]
        public void StartStateTogglesScaleScriptOffIfTwistRotateScriptIsActive()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanTwistRotate>().enabled = false;
            _modelManager.ArModel.AddComponent<LeanPinchScale>().enabled = true;

            // Act
            _startState.ToggleScaleScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanPinchScale>().enabled);
        }

        [Test]
        public void StartStateTogglesMovementScriptCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanDragTranslate>();

            // Act
            _startState.ToggleMovementScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanDragTranslate>().enabled);
        }

        [Test]
        public void StartStateAppliesNoMarksWithoutInput()
        {
            // Arrange
            var modelManager = new Mock<ModelManager>();
            var startState = new Mock<StartState>(modelManager.Object);
            modelManager.Object.ModelState = startState.Object;
            startState.Setup(s => s.CheckIfPhaseEnded());

            // Act
            startState.Object.RegisterModelTouch();

            // Assert
            Assert.IsTrue(modelManager.Object.UniquePixels.Count == 0);
            Assert.IsTrue(modelManager.Object.TextureApplied);
            Assert.IsFalse(modelManager.Object.PhaseStarted);
        }

        [Test]
        public void StartStateManuallyEndsTouchPhase()
        {
            // Arrange
            var modelManager = new Mock<ModelManager>();
            var startState = new Mock<StartState>(modelManager.Object);
            modelManager.Object.ModelState = startState.Object;
            startState.Setup(s => s.ApplyModification());
            startState.Setup(s => s.CheckIfPhaseEnded()).CallBase();

            // Act
            startState.Object.CheckIfPhaseEnded();

            // Assert
            Assert.IsTrue(modelManager.Object.LastPhase == TouchPhase.Ended);
            startState.Verify(s => s.ApplyModification(), Times.Once());
        }

        [Test]
        public void StartStateDeterminesIfTouchPhaseStartedCorrectly()
        {
            // Arrange
            _modelManager.TextureApplied = true;
            _modelManager.PhaseStarted = false;

            // Act
            _startState.CheckIfBeginPhaseHasStarted();

            // Assert
            Assert.IsTrue(_modelManager.RecentPixels.Count == 1);
        }


        [Test]
        public void StartStateDetectsNewTouchPhaseCorrectly()
        {
            // Arrange
            _modelManager.TextureApplied = true;
            _modelManager.PhaseStarted = false;

            // Act
            _startState.CheckIfBeginPhaseHasStarted();

            // Assert
            Assert.IsTrue(_modelManager.RecentPixels.Count == 1);
        }

        [Test]
        public void StartStateDetectsOldTouchPhaseCorrectly()
        {
            // Arrange
            _modelManager.TextureApplied = true;
            _modelManager.PhaseStarted = true;

            // Act
            _startState.CheckIfBeginPhaseHasStarted();

            // Assert
            Assert.IsTrue(!_modelManager.RecentPixels.Any());
        }
                
        [Test]
        public void StartStateCorrectlyRegistersMarkCoordinates()
        {
            // Arrange
            var coord = new Vector2 { x = 10, y = 10 };
            var point = new ModelManager.Point((int)coord.x, (int)coord.y);
            _modelManager.RecentPixels.Push(new List<ModelManager.Point>());
            _modelManager.Texture = new Texture2D(10, 10);

            // Act
            _startState.RegisterMarkCoordinates(coord);

            // Assert
            Assert.IsTrue(_modelManager.UniquePixels.ContainsKey(point));
            Assert.IsTrue(_modelManager.RecentPixels.Peek().Contains(point));
        }


        [Test]
        public void StartStateDoesNotRegisterDuplicateMarkCoordinates()
        {
            // Arrange
            var coord = new Vector2 { x = 10, y = 10 };
            var point = new ModelManager.Point((int)coord.x, (int)coord.y);
            _modelManager.UniquePixels.Add(point, Color.black);
            _modelManager.RecentPixels.Push(new List<ModelManager.Point>());
            _modelManager.Texture = new Texture2D(10, 10);

            // Act
            _startState.RegisterMarkCoordinates(coord);

            // Assert
            Assert.IsTrue(_modelManager.UniquePixels.ContainsKey(point));
            Assert.IsFalse(_modelManager.RecentPixels.Peek().Contains(point));
        }

        [Test]
        public void StartStateRemovesPreviousMarksCorrectly()
        {
            // Arrange
            var point = new ModelManager.Point(10, 10);
            _modelManager.UniquePixels.Add(point, Color.black);
            _modelManager.RecentPixels.Push(new List<ModelManager.Point>());
            _modelManager.RecentPixels.Peek().Add(point);
            _modelManager.Texture = new Texture2D(10, 10);

            // Act
            _startState.ReDo();

            // Assert
            Assert.IsFalse(_modelManager.UniquePixels.ContainsKey(point));
            Assert.IsFalse(_modelManager.RecentPixels.Any());
        }

        [Test]
        public void StartStateAppliesMarkToTheTextureCorrectly()
        {
            // Arrange
            var point = new ModelManager.Point(10, 10);
            _modelManager.RecentPixels.Push(new List<ModelManager.Point>());
            _modelManager.RecentPixels.Peek().Add(point);
            _modelManager.Texture = new Texture2D(10, 10);

            // Act
            _startState.ApplyModification();

            // Assert
            Assert.IsTrue(_modelManager.Texture.GetPixel(10, 10) == Color.red);
        }

        [Test]
        public void StartStateFinishesItsStateCorrectly()
        {
            // Act
            _modelManager.Finish();

            // Assert
            Assert.IsTrue(_modelManager.ModelState != _startState);
        }


        [Test]
        public void StartStateSetsPreviousStateType()
        {
            // Act
            _startState.SetPreviousStateType(ModelStateEnum.Empty);

            // Assert
            Assert.IsTrue(_startState.PreviousState == ModelStateEnum.Empty);
        }


        [Test]
        public void StartStateReturnsPreviousStateType()
        {
            // Arrange
            _startState.PreviousState = ModelStateEnum.Empty;

            // Act
            var previousState = _startState.GetPreviousStateType();

            // Assert
            Assert.IsTrue(previousState == ModelStateEnum.Empty);
        }


        [Test]
        public void StartStateEnablesTransparancyCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<MeshRenderer>();
            // Create temp shader
            _modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));

            // Act
            _startState.EnableTransparancy(true);

            // Assert
            Assert.IsTrue(_modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial.color.a == 0.5f);
        }

        [Test]
        public void StartStateDisablesTransparancyCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<MeshRenderer>();
            // Create temp shader
            _modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));

            // Act
            _startState.EnableTransparancy(false);

            // Assert
            Assert.IsTrue(_modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial.color.a == 1f);
        }
    }
}

