﻿using Assets.Scripts.States;
using NUnit.Framework;
using System;

namespace Assets.Scripts.Editor.ModelManagerTests
{
    public class EmptyStateTest
    {
        private ModelManager _modelManager;
        private EmptyState _emptyState;

        [SetUp]
        public void SetUp()
        {
            // Setup managers
            _modelManager = new ModelManager();
            _emptyState = new EmptyState(_modelManager);
            _modelManager.ModelState = _emptyState;
        }

        [Test]
        public void StartStateReturnsStateType()
        {

            // Act
            var stateType = _emptyState.GetStateType();

            // Assert
            Assert.IsTrue(stateType == ModelStateEnum.Empty);
        }

        [Test]
        public void StartStateSetsPreviousStateType()
        {
            // Act
            _emptyState.SetPreviousStateType(ModelStateEnum.Start);

            // Assert
            Assert.IsTrue(_emptyState.PreviousState == ModelStateEnum.Start);
        }


        [Test]
        public void StartStateReturnsPreviousStateType()
        {
            // Arrange
            _emptyState.PreviousState = ModelStateEnum.Start;

            // Act
            var previousState = _emptyState.GetPreviousStateType();

            // Assert
            Assert.IsTrue(previousState == ModelStateEnum.Start);
        }

        [Test]
        public void StartStateFinishesItsStateCorrectly()
        {
            // Act
            _modelManager.Finish();

            // Assert
            Assert.IsTrue(_modelManager.ModelState != _emptyState);
        }

        [Test]
        public void StartStateDoesNotImplementEnableTransparancy()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.EnableTransparancy(true));
        }

        [Test]
        public void StartStateDoesNotImplementReDo()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.ReDo());
        }

        [Test]
        public void StartStateDoesNotImplementRegisterModelTouch()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.RegisterModelTouch());
        }

        [Test]
        public void StartStateDoesNotImplementResetPos()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.ResetPos());
        }

        [Test]
        public void StartStateDoesNotImplementToggleMarking()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.ToggleMarking());
        }

        [Test]
        public void StartStateDoesNotImplementToggleMovementScript()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.ToggleMovementScript());
        }

        [Test]
        public void StartStateDoesNotImplementToggleRotateScript()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.ToggleRotateScript());
        }

        [Test]
        public void StartStateDoesNotImplementToggleScaleScript()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _emptyState.ToggleScaleScript());
        }
    }
}

