﻿using Assets.Scripts.States;
using Moq;
using NUnit.Framework;
using System.Linq;
using UnityEngine;


namespace Assets.Scripts.Editor.ModelManagerTests
{
    public class ModelManagerTest
    {
        private ModelManager _modelManager;

        [SetUp]
        public void SetUp()
        {
            // Setup manager
            _modelManager = new ModelManager();
        }

        public IModelState GetRandomState()
        {
            var emptyState = new EmptyState(_modelManager);
            var startState = new StartState(_modelManager);
            var finishedState = new FinishedState(_modelManager);
            var list = new IModelState[] { emptyState, startState, finishedState };

            var random = new System.Random();
            return list[random.Next(0, 3)];
        }

        public (ModelStateEnum, IModelState) GetRandomStateEnum()
        {
            var list = new (ModelStateEnum, IModelState)[]
                {
                    (ModelStateEnum.Empty, new EmptyState(_modelManager)),
                    (ModelStateEnum.Start, new StartState(_modelManager)),
                    (ModelStateEnum.Finished, new FinishedState(_modelManager))
                };

            var random = new System.Random();
            return list[random.Next(0, 3)];
        }

        [Test]
        public void ModelManagerCreatesANewStateAtStart()
        {
            // Act
            _modelManager.Start();

            // Assert
            Assert.IsTrue(_modelManager.ModelState.GetType() == typeof(EmptyState)); 
        }

        [Test]
        public void ModelManagerResetPosCallsStateMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(m => m.ResetPos());

            // Act
            _modelManager.ResetPos();

            // Assert
            startState.Verify(m => m.ResetPos(), Times.Once());
        }

        [Test]
        public void ModelManagerToggleMarkingCallsStateMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(m => m.ToggleMarking());

            // Act
            _modelManager.ToggleMarking();

            // Assert
            startState.Verify(m => m.ToggleMarking(), Times.Once());
        }

        [Test]
        public void ModelManagerToggleScaleScriptCallsStateMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(m => m.ToggleScaleScript());

            // Act
            _modelManager.ToggleScaleScript();

            // Assert
            startState.Verify(m => m.ToggleScaleScript(), Times.Once());
        }

        [Test]
        public void ModelManagerToggleRotateScriptCallsStateMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(m => m.ToggleRotateScript());

            // Act
            _modelManager.ToggleRotateScript();

            // Assert
            startState.Verify(m => m.ToggleRotateScript(), Times.Once());
        }

        [Test]
        public void ModelManagerToggleMovementScriptCallsStateMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(m => m.ToggleMovementScript());

            // Act
            _modelManager.ToggleMovementScript();

            // Assert
            startState.Verify(m => m.ToggleMovementScript(), Times.Once());
        }

        [Test]
        public void ModelManagerReDoCallsStateMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(m => m.ReDo());

            // Act
            _modelManager.ReDo();

            // Assert
            startState.Verify(m => m.ReDo(), Times.Once());
        }

        [Test]
        public void ModelManagerAppliesTextureCorrectly()
        {
            // Arrange
            var texture = new Texture2D(10, 10);
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<MeshRenderer>();
            // Create temp shader
            _modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));

            // Act
            _modelManager.ApplyTexture(texture);

            // Assert
            Assert.IsTrue(_modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial.mainTexture == texture);
        }

        [Test]
        public void ModelManagerSetsGameObjectCorrectly()
        {
            // Arrange
            string modelName = "Paul";
            var model = new GameObject("Paul").AddComponent<MeshRenderer>();
            model.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));

            // Act
            _modelManager.SetGameobject(modelName);

            // Assert
            Assert.IsTrue(_modelManager.ArModel.name == modelName);
        }

        [Test]
        public void ModelManagerSetsMeshCorrectly()
        {
            // Arrange
            var mesh = new Mesh();
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<MeshFilter>();
            _modelManager.ArModel.AddComponent<MeshCollider>();

            // Act
            _modelManager.SetMesh(mesh);

            // Assert
            Assert.IsTrue(_modelManager.ArModel.GetComponent<MeshCollider>().sharedMesh == mesh);
        }

        [Test]
        public void ModelManagerSetsTextureCorrectly()
        {
            // Arrange
            var texture = new Texture2D(10, 10);
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<MeshRenderer>();
            // Create temp shader
            _modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));

            // Act
            _modelManager.SetTexture(texture);

            // Assert
            Assert.IsTrue(_modelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial.mainTexture == texture);
        }

        [Test]
        public void ModelManagerGetsTextureCorrectly()
        {
            // Arrange
            var texture = new Texture2D(10, 10);
            _modelManager.Texture = texture;

            // Act
            var textureReturned =_modelManager.GetTexture();

            // Assert
            Assert.IsTrue(textureReturned == texture);
        }

        [Test]
        public void ModelManagerCallsTransparancyMethodCorrectly()
        {
            // Arrange
            var startState = new Mock<StartState>(_modelManager);
            _modelManager.ModelState = startState.Object;
            startState.Setup(s => s.EnableTransparancy(It.IsAny<bool>()));

            // Act
            _modelManager.EnableTransparancy(true);

            // Assert
            startState.Verify(s => s.EnableTransparancy(It.IsAny<bool>()), Times.Once());
        }

        [Test]
        public void ModelManagerResetsToPreviousState()
        {
            // Arrange
            _modelManager.ModelState = GetRandomState();
            var prevStateTuple = GetRandomStateEnum();
            _modelManager.ModelState.SetPreviousStateType(prevStateTuple.Item1);

            // Act
            _modelManager.ResetToPreviousState();

            // Assert
            Assert.IsTrue(_modelManager.ModelState.GetType() == prevStateTuple.Item2.GetType());
        }

        [Test]
        public void ModelManagerStartWithEmptyState()
        {
            // Arrange
            _modelManager.ModelState = GetRandomState();

            // Act
            _modelManager.ResetState();

            // Assert
            Assert.IsTrue(_modelManager.ModelState.GetType() == typeof(EmptyState));
            Assert.IsTrue(!_modelManager.UniquePixels.Any());
            Assert.IsTrue(!_modelManager.RecentPixels.Any());
        }
    }     
}
