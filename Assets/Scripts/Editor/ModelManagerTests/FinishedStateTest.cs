﻿using Assets.Scripts.States;
using Lean.Touch;
using NUnit.Framework;
using System;
using UnityEngine;

namespace Assets.Scripts.Editor.ModelManagerTests
{
    public class FinishedStateTest
    {
        private ModelManager _modelManager;
        private FinishedState _finishedState;

        [SetUp]
        public void SetUp()
        {
            // Setup managers
            _modelManager = new ModelManager();
            _finishedState = new FinishedState(_modelManager);
            _modelManager.ModelState = _finishedState;
        }

        [Test]
        public void FinishedStateReturnsStateType()
        {

            // Act
            var stateType = _finishedState.GetStateType();

            // Assert
            Assert.IsTrue(stateType == ModelStateEnum.Finished);
        }

        [Test]
        public void FinishedStateSetsPreviousStateType()
        {
            // Act
            _finishedState.SetPreviousStateType(ModelStateEnum.Start);

            // Assert
            Assert.IsTrue(_finishedState.PreviousState == ModelStateEnum.Start);
        }


        [Test]
        public void FinishedStateReturnsPreviousStateType()
        {
            // Arrange
            _finishedState.PreviousState = ModelStateEnum.Start;

            // Act
            var previousState = _finishedState.GetPreviousStateType();

            // Assert
            Assert.IsTrue(previousState == ModelStateEnum.Start);
        }

        [Test]
        public void FinishedStateFinishesItsStateCorrectly()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _finishedState.Finish());
        }

        [Test]
        public void FinishedStateDoesNotImplementEnableTransparancy()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _finishedState.EnableTransparancy(true));
        }

        [Test]
        public void FinishedStateDoesNotImplementReDo()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _finishedState.ReDo());
        }

        [Test]
        public void FinishedStateDoesNotImplementRegisterModelTouch()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _finishedState.RegisterModelTouch());
        }

        [Test]
        public void StartStateResetsPositionCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();

            // Act
            _finishedState.ResetPos();

            // Assert
            Assert.AreEqual(_modelManager.ArModel.transform.localScale, new Vector3(0.45f, 0.45f, 0.45f));
        }

        [Test]
        public void StartStateResetsPositionCorrectlyWithExceptions()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<Transform>();
            _modelManager.ArModel.transform.name = "Baron";

            // Act
            _finishedState.ResetPos();

            // Assert
            Assert.AreEqual(_modelManager.ArModel.transform.localScale, new Vector3(0.2f, 0.2f, 0.2f));
        }

        [Test]
        public void FinishedStateDoesNotImplementToggleMarking()
        {
            // Assert
            Assert.Throws<NotImplementedException>(() => _finishedState.ToggleMarking());
        }

        [Test]
        public void StartStateTogglesTwistRotateScriptOffIfScaleScriptIsActive()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanPinchScale>().enabled = false;
            _modelManager.ArModel.AddComponent<LeanTwistRotate>().enabled = true;

            // Act
            _finishedState.ToggleScaleScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanTwistRotate>().enabled);
        }

        [Test]
        public void StartStateTogglesTwistRotateScriptCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanTwistRotate>();

            // Act
            _finishedState.ToggleRotateScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanTwistRotate>().enabled);
        }

        [Test]
        public void StartStateTogglesScaleScriptOffIfTwistRotateScriptIsActive()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanTwistRotate>().enabled = false;
            _modelManager.ArModel.AddComponent<LeanPinchScale>().enabled = true;

            // Act
            _finishedState.ToggleScaleScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanPinchScale>().enabled);
        }

        [Test]
        public void StartStateTogglesMovementScriptCorrectly()
        {
            // Arrange
            _modelManager.ArModel = new GameObject();
            _modelManager.ArModel.AddComponent<LeanDragTranslate>();

            // Act
            _finishedState.ToggleMovementScript();

            // Assert
            Assert.IsFalse(_modelManager.ArModel.GetComponent<LeanDragTranslate>().enabled);
        }
    }
}

