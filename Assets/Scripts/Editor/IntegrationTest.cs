﻿using Assets.Scripts.States;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class IntegrationTest
    {

        private Mock<MenuManager> _menuManager;
        private Mock<FileManager> _fileManager;
        private Mock<ModelManager> _modelManager;
        private StartState _startState;

         [SetUp]
        public void SetUp()
        {
            // Setup managers
            _menuManager = new Mock<MenuManager>();
            _fileManager = new Mock<FileManager>();
            _modelManager = new Mock<ModelManager>();
            _startState = new StartState(_modelManager.Object);
            _modelManager.Object.ModelState = _startState;

        // Set variables
        _menuManager.Object.FileManager = _fileManager.Object;
            _menuManager.Object.ModelManager = _modelManager.Object;
        }


        [Test]
        public void LoadEditAndSaveAModel()
        {
            // Arrange
            // Set recognized GameObject (unable to Mock)
            _modelManager.Object.ArModel = new GameObject();
            _modelManager.Object.ArModel.AddComponent<MeshRenderer>();
            _modelManager.Object.ArModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));
            // Create and fill dropdown
            _menuManager.Object._textureOptions = new List<string> { DropdownEnum.Original, DropdownEnum.Modified, DropdownEnum.None };
            var textureDropdown = new GameObject().AddComponent<Dropdown>();
            textureDropdown.AddOptions(_menuManager.Object._textureOptions);
            _menuManager.Object.TextureDropdown = textureDropdown;
            textureDropdown.value = 0;
            // Fill list with mark coordinate
            var point = new ModelManager.Point(10, 10);
            _modelManager.Object.RecentPixels.Push(new List<ModelManager.Point>());
            _modelManager.Object.RecentPixels.Peek().Add(point);
            // Add listeners to dropdown
            textureDropdown.onValueChanged.AddListener(delegate
            {
                _menuManager.Object.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.TextureDropdown, "");
            });
            // Call base methods for certain methods
            // Mock all FTP and input by touch (unable to mock) related methods
            _menuManager.Setup(m => m.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.TextureDropdown, "")).CallBase();
            _fileManager.Setup(f => f.LoadTexture(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Returns(new Texture2D(10, 10));
            _menuManager.Setup(m => m.DropdownValueChanged(It.IsAny<string>(), It.IsAny<Dropdown>(), It.IsAny<string>())).CallBase();
            _modelManager.Setup(m => m.SetTexture(It.IsAny<Texture2D>())).CallBase();
            _menuManager.Setup(m => m.SaveTexture()).CallBase();
            _fileManager.Setup(f => f.SaveToPNG(It.IsAny<Texture2D>(), It.IsAny<string>(), It.IsAny<string>())).CallBase();
            _menuManager.Setup(m => m.FillTextureDropdown()).CallBase();
            _fileManager.Setup(f => f.SaveWithFTP(It.IsAny<string>(), It.IsAny<byte[]>()));
            _fileManager.Setup(f => f.IsModifiedTextureAvailable(It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            
            // Act
            // --Load
            _menuManager.Object.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.TextureDropdown, "");
            // Unity input cannot be mocked and thus StartState.RegisterModelTouch() is skipped while testing
            // Manually set Texture
            _modelManager.Object.Texture = (Texture2D)_modelManager.Object.ArModel.GetComponent<MeshRenderer>().sharedMaterial.mainTexture;
            // --Edit
            _startState.ApplyModification();
            // --Save
            _menuManager.Object.SaveTexture();

            // Assert
            // --Load
            _menuManager.Verify(m => m.DropdownValueChanged(It.IsAny<string>(), It.IsAny<Dropdown>(), It.IsAny<string>()), Times.Exactly(3));
            // --Edit
            Assert.IsTrue(_modelManager.Object.Texture.GetPixel(10, 10) == Color.red);
            // --Save
            _fileManager.Verify(f => f.SaveWithFTP(It.IsAny<string>(), It.IsAny<byte[]>()), Times.Once());
        }

        [Test]
        public void ModelManagerCanGoThroughAllModelStates()
        {
            // Arrange
            _modelManager.Setup(m => m.ResetState()).CallBase();
            _modelManager.Setup(m => m.ReDo()).CallBase();
            _modelManager.Setup(m => m.Finish()).CallBase();
            _modelManager.Object.ResetState();
            // Act & Assert
            // -- Empty state
            Assert.Throws<NotImplementedException>(() => _modelManager.Object.ReDo());
            Assert.IsTrue(_modelManager.Object.ModelState.GetStateType() == ModelStateEnum.Empty);
            _modelManager.Object.Finish();
            // -- Start state
            Assert.IsTrue(_modelManager.Object.ModelState.GetStateType() == ModelStateEnum.Start);
            Assert.IsTrue(_modelManager.Object.ModelState.GetPreviousStateType() == ModelStateEnum.Empty);
            _modelManager.Object.Finish();
            // -- Finished state
            Assert.Throws<NotImplementedException>(() => _modelManager.Object.ReDo());
            Assert.IsTrue(_modelManager.Object.ModelState.GetStateType() == ModelStateEnum.Finished);
            Assert.IsTrue(_modelManager.Object.ModelState.GetPreviousStateType() == ModelStateEnum.Start);
            Assert.Throws<NotImplementedException>(() => _modelManager.Object.Finish());
        }
    }
}
