﻿using Assets.Scripts.States;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Editor
{
    public class MenuManagerTest
    {
        private Mock<MenuManager> _menuManager;
        private Mock<FileManager> _fileManager;
        private Mock<ModelManager> _modelManager;

        [SetUp]
        public void SetUp()
        {
            // Setup managers
            _menuManager = new Mock<MenuManager>();
            _fileManager = new Mock<FileManager>();
            _modelManager = new Mock<ModelManager>();

            // Set variables
            _menuManager.Object.FileManager = _fileManager.Object;
            _menuManager.Object.ModelManager = _modelManager.Object;
        }

        [Test]
        public void MainMenuReactsToVersionValueChangeAndAnalysisCompletedCorrectly()
        {
            // Arrange
            _menuManager.Object._versionOptions = new List<string> { "", "" };
            var versionDropdown = new GameObject().AddComponent<Dropdown>();
            versionDropdown.AddOptions(_menuManager.Object._versionOptions);
            versionDropdown.value = 0;
            _menuManager.Object.VersionDropdown = versionDropdown;
            var analysisCompletedToggle = new GameObject("AnalysisCompletedToggle").AddComponent<Toggle>();
            var saveBtn = new GameObject("SaveBtn").AddComponent<Button>();

            // Add listeners
            versionDropdown.onValueChanged.AddListener(delegate
            {
                _menuManager.Object.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, "");
            });

            // Mock all methods that are called
            _menuManager.Setup(m => m.FillTextureDropdown());
            _modelManager.Setup(m => m.ResetState());
            _modelManager.Setup(m => m.SetMesh(null));
            _modelManager.Setup(m => m.Finish());
            _fileManager.Setup(f => f.LoadMesh("", "")).Returns(new Mesh());
            _fileManager.Setup(f => f.IsDamageAnalysisCompleted("")).Returns((int)ModelStateEnum.Finished);
            _menuManager.Setup(m => m.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, "")).CallBase();

            // Act
            _menuManager.Object.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, "");

            // Assert
            Assert.IsTrue(_menuManager.Object._damageAnalysisCompleted);
            Assert.IsFalse(GameObject.Find("SaveBtn").GetComponent<Button>().interactable);
            _menuManager.Verify(m => m.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, ""), Times.Once());
            _modelManager.Verify(m => m.ResetState(), Times.Once());
            _menuManager.Verify(m => m.FillTextureDropdown(), Times.Once());
        }

        [Test]
        public void MainMenuReactsToVersionValueChangeAndAnalysisInProgressCorrectly()
        {
            // Arrange
            _menuManager.Object._versionOptions = new List<string> { "", "" };
            var versionDropdown = new GameObject().AddComponent<Dropdown>();
            versionDropdown.AddOptions(_menuManager.Object._versionOptions);
            versionDropdown.value = 0;
            _menuManager.Object.VersionDropdown = versionDropdown;
            var analysisCompletedToggle = new GameObject("AnalysisCompletedToggle").AddComponent<Toggle>();
            var saveBtn = new GameObject("SaveBtn").AddComponent<Button>();

            // Add listeners
            versionDropdown.onValueChanged.AddListener(delegate
            {
                _menuManager.Object.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, "");
            });

            // Mock all methods that are called
            _menuManager.Setup(m => m.FillTextureDropdown());
            _modelManager.Setup(m => m.ResetState());
            _modelManager.Setup(m => m.SetMesh(null));
            _modelManager.Setup(m => m.Finish());
            _fileManager.Setup(f => f.LoadMesh("", "")).Returns(new Mesh());
            _fileManager.Setup(f => f.IsDamageAnalysisCompleted("")).Returns((int)ModelStateEnum.Start);
            _menuManager.Setup(m => m.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, "")).CallBase();

            // Act
            _menuManager.Object.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, "");

            // Assert
            Assert.IsFalse(_menuManager.Object._damageAnalysisCompleted);
            Assert.IsTrue(GameObject.Find("SaveBtn").GetComponent<Button>().interactable);
            _menuManager.Verify(m => m.DropdownValueChanged(DropdownEnum.Version, _menuManager.Object.VersionDropdown, ""), Times.Once());
            _modelManager.Verify(m => m.ResetState(), Times.Once());
            _menuManager.Verify(m => m.FillTextureDropdown(), Times.Once());
        }

        [Test]
        public void MainMenuReactsToTextureValueChangeCorrectly()
        {
            // Arrange
            _menuManager.Object._textureOptions = new List<string> { DropdownEnum.Original, DropdownEnum.Modified, DropdownEnum.None };
            var textureDropdown = new GameObject().AddComponent<Dropdown>();
            textureDropdown.AddOptions(_menuManager.Object._textureOptions);
            _menuManager.Object.TextureDropdown = textureDropdown;

            // Add listeners
            textureDropdown.onValueChanged.AddListener(delegate
            {
                _menuManager.Object.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.TextureDropdown, "");
            });

            // Call base method
            _menuManager.Setup(m => m.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.TextureDropdown, "")).CallBase();
            _fileManager.Setup(f => f.LoadTexture("", "", It.IsAny<bool>())).Returns(new Texture2D(10, 10));
            _modelManager.Setup(m => m.SetTexture(It.IsAny<Texture2D>()));

            // Act
            textureDropdown.value = 0;
            _menuManager.Object.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.VersionDropdown, "");
            textureDropdown.value = 1;
            _menuManager.Object.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.VersionDropdown, "");
            textureDropdown.value = 2;
            _menuManager.Object.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.VersionDropdown, "");

            // Assert
            _menuManager.Verify(m => m.DropdownValueChanged(DropdownEnum.Texture, _menuManager.Object.VersionDropdown, ""), Times.Exactly(3));
            _modelManager.Verify(m => m.SetTexture(It.IsAny<Texture2D>()), Times.Exactly(3));
        }

        [Test]
        public void MainMenuCanBeToggled()
        {
            // Arrange
            var menuManager = new GameObject().AddComponent<MenuManager>();
            menuManager.MainMenu = new GameObject();
            menuManager.ExpandBtn = new GameObject();
            var active = menuManager.MainMenu.activeSelf;

            // Act
            menuManager.SetActive();

            // Assert
            Assert.AreEqual(!active, menuManager.MainMenu.activeSelf);
        }

        [Test]
        public void SelectingAVersionLoadsAllVersionFileNames()
        {
            // Arrange
            _menuManager.Object.VersionDropdown = new GameObject().AddComponent<Dropdown>();
            // Mock retrieving files from server
            _fileManager.Setup(f => f.GetModelVersions(null))
                .Returns(new List<string>() { "1", "2", "3" });
            _menuManager.Setup(m => m.DropdownValueChanged(null, null, null));

            // Act
            _menuManager.Object.FillVersionDropdown(null);

            // Assert
            Assert.IsTrue(_menuManager.Object.VersionDropdown.options.Count == 4);
        }

        [Test]
        public void SelectingATextureLoadsAllTextureFileNamesIncludingModified()
        {
            // Arrange
            _menuManager.Object.TextureDropdown = new GameObject().AddComponent<Dropdown>();
            // Mock retrieving files from server
            _fileManager.Setup(f => f.IsModifiedTextureAvailable(null, null))
                .Returns(true);
            _menuManager.Setup(m => m.DropdownValueChanged(null, null, null));
            _menuManager.Setup(m => m.FillTextureDropdown()).CallBase();

            // Act
            _menuManager.Object.FillTextureDropdown();

            // Assert
            Assert.IsTrue(_menuManager.Object.TextureDropdown.options.Count == 4);
        }

        [Test]
        public void SelectingATextureLoadsAllTextureFileNamesExcludingModified()
        {
            // Arrange
            _menuManager.Object.TextureDropdown = new GameObject().AddComponent<Dropdown>();
            // Mock retrieving files from server
            _fileManager.Setup(f => f.IsModifiedTextureAvailable(null, null))
                .Returns(false);
            _menuManager.Setup(m => m.DropdownValueChanged(null, null, null));
            _menuManager.Setup(m => m.FillTextureDropdown()).CallBase();

            // Act
            _menuManager.Object.FillTextureDropdown();

            // Assert
            Assert.IsTrue(_menuManager.Object.TextureDropdown.options.Count == 3);
        }

        [Test]
        public void PressingSaveBtnCallsSaveMethods()
        {
            // Arrange
            _menuManager.Object.TextureDropdown = new GameObject().AddComponent<Dropdown>();
            var texture = new Texture2D(10, 10);

            _modelManager.Setup(m => m.GetTexture()).Returns(texture);
            _fileManager.Setup(f => f.SaveToPNG(texture, null, null));
            _menuManager.Setup(m => m.FillTextureDropdown());
            _menuManager.Setup(m => m.SaveTexture()).CallBase();

            // Act
            _menuManager.Object.SaveTexture();

            // Verify
            _fileManager.Verify(f => f.SaveToPNG(texture, null, null), Times.Once());
            _menuManager.Verify(m => m.FillTextureDropdown(), Times.Once());
        }

        [Test]
        public void ToggleTransparancyBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _modelManager.Setup(m => m.EnableTransparancy(It.IsAny<bool>()));

            // Act
            _menuManager.Object.ToggleTransparancy(true);

            // Assert
            _modelManager.Verify(m => m.EnableTransparancy(true), Times.Once());
        }

        [Test]
        public void ResetPositionBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _modelManager.Setup(m => m.ResetPos());

            // Act
            _menuManager.Object.ResetPos();

            // Assert
            _modelManager.Verify(m => m.ResetPos(), Times.Once());
        }

        [Test]
        public void ToggleMarkingBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _modelManager.Setup(m => m.ToggleMarking()).Returns(true);
            var markBtn = new GameObject("MarkBtn").AddComponent<Image>();

            // Act
            _menuManager.Object.ToggleMarking();

            // Assert
            _modelManager.Verify(m => m.ToggleMarking(), Times.Once());
            Assert.IsTrue(GameObject.Find("MarkBtn").GetComponent<Image>().color == Color.green);
        }

        [Test]
        public void ToggleScalingBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _modelManager.Setup(m => m.ToggleScaleScript()).Returns(true);
            var rotateBtn = new GameObject("RotateBtn").AddComponent<Image>();
            var scaleBtn = new GameObject("ScaleBtn").AddComponent<Image>();

            // Act
            _menuManager.Object.ToggleScaling();

            // Assert
            _modelManager.Verify(m => m.ToggleScaleScript(), Times.Once());
            Assert.IsTrue(GameObject.Find("ScaleBtn").GetComponent<Image>().color == Color.green);
            Assert.IsTrue(GameObject.Find("RotateBtn").GetComponent<Image>().color == Color.red);
        }

        [Test]
        public void ToggleRotatingBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _modelManager.Setup(m => m.ToggleRotateScript()).Returns(true);
            var rotateBtn = new GameObject("RotateBtn").AddComponent<Image>();
            var scaleBtn = new GameObject("ScaleBtn").AddComponent<Image>();

            // Act
            _menuManager.Object.ToggleRotating();

            // Assert
            _modelManager.Verify(m => m.ToggleRotateScript(), Times.Once());
            Assert.IsTrue(GameObject.Find("RotateBtn").GetComponent<Image>().color == Color.green);
            Assert.IsTrue(GameObject.Find("ScaleBtn").GetComponent<Image>().color == Color.red);
        }

        [Test]
        public void ToggleModelMovementBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _modelManager.Setup(m => m.ToggleMovementScript()).Returns(true);
            var lockBtn = new GameObject("LockBtn").AddComponent<Image>();

            // Act
            _menuManager.Object.LockMovement();

            // Assert
            _modelManager.Verify(m => m.ToggleMovementScript(), Times.Once());
            Assert.IsTrue(GameObject.Find("LockBtn").GetComponent<Image>().color == Color.green);
        }

        [Test]
        public void ToggleDamageAnalysisCompletionTrueBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _menuManager.Object._damageAnalysisCompleted = false;
            _menuManager.Setup(m => m.ToggleDamageAnalysisCompletion(true)).CallBase();
            _fileManager.Setup(f => f.SetDamageAnalysisCompletion("", ModelStateEnum.Finished));
            var saveBtn = new GameObject("SaveBtn").AddComponent<Button>();

            // Act
            _menuManager.Object.ToggleDamageAnalysisCompletion(true);

            // Assert
            _modelManager.Verify(m => m.Finish(), Times.Once());
        }

        [Test]
        public void ToggleDamageAnalysisCompletionFalseBtnCallsModelManagerCorrectly()
        {
            // Arrange
            _menuManager.Object._damageAnalysisCompleted = true;
            _menuManager.Setup(m => m.ToggleDamageAnalysisCompletion(false)).CallBase();
            _modelManager.Setup(m => m.ResetToPreviousState());
            _fileManager.Setup(f => f.SetDamageAnalysisCompletion("", ModelStateEnum.Finished));
            var lockBtn = new GameObject("LockBtn").AddComponent<Image>();

            // Act
            _menuManager.Object.ToggleDamageAnalysisCompletion(false);

            // Assert
            _modelManager.Verify(m => m.ResetToPreviousState(), Times.Once());
        }

        [Test]
        public void MenuManagerIgnoresInitialDamageAnalysisManualUpdate()
        {
            // Arrange
            _menuManager.Object._damageAnalysisCompleted = true;
            _menuManager.Setup(m => m.ToggleDamageAnalysisCompletion(true)).CallBase();
            _modelManager.Setup(m => m.ResetToPreviousState());

            // Act
            _menuManager.Object.ToggleDamageAnalysisCompletion(true);

            // Assert
            _modelManager.Verify(m => m.ResetToPreviousState(), Times.Exactly(0));
        }
    }
}
