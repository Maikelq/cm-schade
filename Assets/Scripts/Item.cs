﻿using System.Xml;
using System.Xml.Serialization;

namespace Assets.Scripts
{
    public partial class FileManager
    {
        /// <summary>
        ///     Used for serializing dictionary
        /// </summary>
        public class Item
        {
            [XmlAttribute] public string Id;
            [XmlAttribute] public int Value;
        }
    }
}