﻿using Assets.Scripts.States;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    ///     Handles retrieving meshes and textures from external sources
    /// </summary>
    public partial class FileManager : MonoBehaviour
    {
        public Dictionary<string, int> _modelStates = new Dictionary<string, int>();

        /// <summary>
        ///     Load a certain mesh
        /// </summary>
        public virtual Mesh LoadMesh(string modelName, string version)
        {
            // load the correct file --to be improved
            // loading files from external sources does not work sometimes due to large files(10mb +) freezing the main thread forever
            return (Mesh) Resources.Load(Configuration.MeshPath + modelName + "/" + version, typeof(Mesh));
        }

        /// <summary>
        ///     Loads a certain texture
        /// </summary>
        public virtual Texture2D LoadTexture(string modelName, string version, bool original)
        {
            var fileEnding = original ? "_tex.png" : "_tex_modified.png";

            var bytes = DownloadWithFTP(Configuration.TexturePath + modelName + "/" + version + fileEnding);
            var texture = new Texture2D(0, 0);
            texture.LoadImage(bytes);
            return texture;
        }

        /// <summary>
        ///     Save current modifications
        /// </summary>
        public virtual void SaveToPNG(Texture2D texture, string modelName, string version)
        {
            SaveWithFTP(Configuration.TexturePath + modelName + "/" + version + Configuration.ModifiedName, texture.EncodeToPNG());
        }

        /// <summary>
        ///     Returns all versions (meshes) of a single model
        /// </summary>
        public virtual List<string> GetModelVersions(string modelName)
        {
            var request = CreateWebRequest(Configuration.FTPHost + Configuration.MeshPath + modelName + "/");
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

            var availableFiles = new List<string>();
            string[] list;

            // Read response
            using (var response = GetWebResponse(request))
            using (var reader = new StreamReader(GetWebResponseStream(response) ?? throw new InvalidOperationException()))
            {
                list = reader.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }

            // Response will be in XML format (Windows), remove all unnecessary fields
            foreach (var line in list)
            {
                var data = line;
                data = data.Remove(0, 24);
                data = data.Remove(0, 5);
                data = data.Remove(0, 10);
                data = data.Remove(data.IndexOf(".", StringComparison.Ordinal));
                availableFiles.Add(data);
            }
            return availableFiles;
        }

        /// <summary>
        ///     Check if a model has a modified texture file
        /// </summary>
        public virtual bool IsModifiedTextureAvailable(string modelName, string version)
        {
            var request = CreateWebRequest(Configuration.FTPHost + Configuration.TexturePath + modelName + "/" + version + Configuration.ModifiedName);
            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;

            // Try to retrieve file
            try
            {
                GetWebResponse(request);
                return true;
            }
            catch (WebException ex)
            {
                var response = (FtpWebResponse) ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
            }
            return false;
        }

        /// <summary>
        ///     Sets damage analysis completion value
        /// </summary>
        public virtual void SetDamageAnalysisCompletion(string version, ModelStateEnum state)
        {
            // Set new value in dictionary
            _modelStates[version] = (int) state;

            var xmlDocument = SerializeObject(_modelStates);

            if (xmlDocument != null)
            {
                var request = CreateWebRequest(Configuration.FTPHost + "test.xml");
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // Save xml file
                using (var requestStream = GetWebRequestStream(request))
                {
                    xmlDocument.Save(requestStream);
                    requestStream.Close();
                }
            }
        }

        /// <summary>
        ///     Check if damage analysis is completed
        /// </summary>
        public virtual int IsDamageAnalysisCompleted(string version)
        {
            var request = CreateWebRequest(Configuration.FTPHost + "test.xml");
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            // Download XML file and deserialize Object
            DeserializeObject(request);

            // Value = ModelStateEnum
            _modelStates.TryGetValue(version, out var value);
            // Version does not exist, so create new entry
            if (value == 0)
            {
                SetDamageAnalysisCompletion(version, ModelStateEnum.Start);
                return (int) ModelStateEnum.Start;
            }
            return value;
        }

        /// <summary>
        ///     Download file with FTP
        /// </summary>
        public virtual byte[] DownloadWithFTP(string fileLoc)
        {
            var request = CreateWebRequest(Configuration.FTPHost + fileLoc);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (var input = GetWebResponseStream(GetWebResponse(request)))
            {
                // Retrieve file in bytes
                var buffer = new byte[16 * 1024];
                using (var ms = new MemoryStream())
                {
                    int read;
                    while (input != null && (input.CanRead && (read = input.Read(buffer, 0, buffer.Length)) > 0))
                        ms.Write(buffer, 0, read);

                    return ms.ToArray();
                }
            }
        }

        /// <summary>
        ///     Save a file to FTP-server
        /// </summary>
        public virtual void SaveWithFTP(string filePath, byte[] bytes)
        {
            var request = CreateWebRequest(Configuration.FTPHost + filePath);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // Send file in bytes
            request.ContentLength = bytes.Length;
            using (var requestStream = GetWebRequestStream(request))
            {
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
            }
        }

        /// <summary>
        ///     Serializes an object.
        /// </summary>
        public virtual XmlDocument SerializeObject(Dictionary<string, int> dict)
        {
            if (dict == null) return null;

            try
            {
                var xmlDocument = new XmlDocument();

                var serializer =
                    new XmlSerializer(typeof(Item[]), new XmlRootAttribute {ElementName = "items"});

                using (var stream = new MemoryStream())
                {
                    // Convert dictionary Key value pairs to Item
                    serializer.Serialize(stream,
                        dict.Select(kv => new Item {Id = kv.Key, Value = kv.Value}).ToArray());

                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    return xmlDocument;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///     Deserializes an object.
        /// </summary>
        public virtual void DeserializeObject(FtpWebRequest request)
        {
            // Download XML file and deserialize object
            var serializer = new XmlSerializer(typeof(Item[]),
                new XmlRootAttribute {ElementName = "items"});
            using (var stream = GetWebResponseStream(GetWebResponse(request)))
            {
                _modelStates = ((Item[]) serializer.Deserialize(stream ?? throw new InvalidOperationException())).ToDictionary(i => i.Id, i => i.Value);
                stream.Close();
            }
        }

        /// <summary>
        /// creates a FtpWebRequest
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public virtual FtpWebRequest CreateWebRequest(string uri)
        {
            // Set up FTP connection
            var request = (FtpWebRequest)WebRequest.Create(new Uri(uri));
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential(Configuration.FTPUsername, Configuration.FTPPassword);
            return request;
        }

        /// <summary>
        /// Sends the request
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns a response from an Internet resource</returns>
        public virtual FtpWebResponse GetWebResponse(FtpWebRequest request )
        {
            return (FtpWebResponse) request.GetResponse();
        }

        /// <summary>
        /// Gets the stream that is used to read the body of the response from the server
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public virtual Stream GetWebResponseStream(FtpWebResponse response)
        {
            return response.GetResponseStream();
        }

        /// <summary>
        /// Gets a Stream object to use to write request data.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public virtual Stream GetWebRequestStream(FtpWebRequest request)
        {
            return request.GetRequestStream();
        }
    }
}