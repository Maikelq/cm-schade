﻿namespace Assets.Scripts
{
    public partial class ModelManager
    {
        /// <summary>
        ///     Used to save 2 coordinates when applying modifications
        /// </summary>
        public struct Point
        {
            public int X, Y;

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
    }
}