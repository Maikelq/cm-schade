﻿using Assets.Scripts.States;
using Vuforia;

namespace Assets.Scripts
{
    public class MyDefaultTrackableEventHandler : DefaultTrackableEventHandler
    {
        public static string CurrentModel;
        public MenuManager MenuManager;
        public ModelManager ModelManager;

        /// <summary>
        ///     Sets the correct gameobject when a physical object is recognized
        /// </summary>
        protected override void OnTrackingFound()
        {
            // The current model should be finished before analyzing the next model
            if (CurrentModel != mTrackableBehaviour.Trackable.Name &&
                ModelManager.UniquePixels.Count == 0)
            {
                // Set current model
                CurrentModel = mTrackableBehaviour.Trackable.Name;
                // Load dropdown showing all available versions
                MenuManager.FillVersionDropdown(mTrackableBehaviour.Trackable.Name);
                base.OnTrackingFound();
            }
            else if (CurrentModel == mTrackableBehaviour.Trackable.Name)
            {
                base.OnTrackingFound();
            }

            // Set camera to use autofocus
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }
}