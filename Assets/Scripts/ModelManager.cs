using System.Collections.Generic;
using Assets.Scripts.States;
using UnityEngine;
using Color = UnityEngine.Color;

namespace Assets.Scripts
{
    public partial class ModelManager : MonoBehaviour
    {
        public GameObject ArModel;
        public TouchPhase LastPhase;
        public bool MarkingEnabled = true;
        public IModelState ModelState;
        public bool PhaseEnded = false;
        public bool PhaseStarted = false;
        public Stack<List<Point>> RecentPixels = new Stack<List<Point>>();
        public Texture2D Texture;
        public bool TextureApplied = true;
        public Dictionary<Point, Color> UniquePixels = new Dictionary<Point, Color>();


        /// <summary>
        ///     Is called once at the start
        /// </summary>
        public void Start()
        {
            ModelState = new EmptyState(this);
        }

        /// <summary>
        ///     Checking for updates
        /// </summary>
        private void FixedUpdate()
        {
            if (MarkingEnabled) ModelState.RegisterModelTouch();
        }

        /// <summary>
        ///     Resets the position and size of the model
        /// </summary>
        public virtual void ResetPos()
        {
            ModelState.ResetPos();
        }

        /// <summary>
        ///     Enable and disables the ability to mark the 3D-model's texture
        /// </summary>
        /// <returns> 3D-model marking active </returns>
        public virtual bool ToggleMarking()
        {
            return ModelState.ToggleMarking();
        }

        /// <summary>
        ///     Enable and disable scaling script
        /// </summary>
        /// <returns> pinch script active </returns>
        public virtual bool ToggleScaleScript()
        {
            return ModelState.ToggleScaleScript();
        }

        /// <summary>
        ///     Enable and disable rotating script
        /// </summary>
        /// <returns> Rotating script active </returns>
        public virtual bool ToggleRotateScript()
        {
            return ModelState.ToggleRotateScript();
        }

        /// <summary>
        ///     Lock 3D-model movement
        /// </summary>
        /// <returns> Script active </returns>
        public virtual bool ToggleMovementScript()
        {
            return ModelState.ToggleMovementScript();
        }

        /// <summary>
        ///     Restore previous modifications
        /// </summary>
        public virtual void ReDo()
        {
            ModelState.ReDo();
        }

        /// <summary>
        ///     Apply texture to current mesh
        /// </summary>
        /// <param name="texture"> Texture that will be applied to mesh </param>
        public void ApplyTexture(Texture2D texture)
        {
            ArModel.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;
        }

        /// <summary>
        ///     Set Gameobject so other components can access it
        /// </summary>
        /// <param name="modelName"> Name of the model </param>
        public virtual void SetGameobject(string modelName)
        {
            ArModel = GameObject.Find(modelName);
            ArModel.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = null;
        }

        /// <summary>
        ///     Apply mesh to the current model gameobject
        /// </summary>
        /// <param name="mesh"> Mesh that will be applied </param>
        public virtual void SetMesh(Mesh mesh)
        {
            ArModel.GetComponent<MeshFilter>().mesh = mesh;
            ArModel.GetComponent<MeshCollider>().sharedMesh = mesh;
        }

        /// <summary>
        ///     Apply texture to the current mesh
        /// </summary>
        /// <param name="texture"> texture that will be applied </param>
        public virtual void SetTexture(Texture2D texture)
        {
            ArModel.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;
        }

        /// <summary>
        ///     Return current texture
        /// </summary>
        public virtual Texture2D GetTexture()
        {
            return Texture;
        }

        /// <summary>
        ///     Set rendering mode to transparent
        /// </summary>
        public virtual void EnableTransparancy(bool value)
        {
            ModelState.EnableTransparancy(value);
        }

        /// <summary>
        ///     Finish current state
        /// </summary>
        public virtual void Finish()
        {
            ModelState = ModelState.Finish();
        }

        /// <summary>
        ///     Reset to previous state
        /// </summary>
        public virtual void ResetToPreviousState()
        {
            switch (ModelState.GetPreviousStateType())
            {
                case ModelStateEnum.Empty:
                    ModelState = new EmptyState(this);
                    break;
                case ModelStateEnum.Start:
                    ModelState = new StartState(this);
                    break;
                case ModelStateEnum.Finished:
                    ModelState = new FinishedState(this);
                    break;
            }
        }

        /// <summary>
        ///     Restart with an empty state
        /// </summary>
        public virtual void ResetState()
        {
            UniquePixels = new Dictionary<Point, Color>();
            RecentPixels = new Stack<List<Point>>();
            ModelState = new EmptyState(this);
        }
    }
}