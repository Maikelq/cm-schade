﻿using System.Collections.Generic;
using Assets.Scripts.States;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class MenuManager : MonoBehaviour
    {
        private string _currentModel;
        private string _currentVersion;
        public bool _damageAnalysisCompleted;
        public List<string> _textureOptions;
        public List<string> _versionOptions;
        public GameObject ExpandBtn;
        public FileManager FileManager;
        public GameObject MainMenu;
        public ModelManager ModelManager;
        public Dropdown TextureDropdown;
        public Dropdown VersionDropdown;

        /// <summary>
        ///     Is called once at the start
        /// </summary>
        public void Start()
        {
            // Add listener to change mesh when user selects a different version
            VersionDropdown.onValueChanged.AddListener(delegate
            {
                DropdownValueChanged(DropdownEnum.Version, VersionDropdown, _currentModel);
            });

            // Add listener to change texture when user selects a different texture option
            TextureDropdown.onValueChanged.AddListener(delegate
            {
                DropdownValueChanged(DropdownEnum.Texture, TextureDropdown, _currentModel);
            });
        }

        /// <summary>
        ///     Collapse or expand main menu
        /// </summary>
        public void SetActive()
        {
            var active = !MainMenu.activeSelf;
            MainMenu.SetActive(active);
            ExpandBtn.SetActive(!active);
        }

        /// <summary>
        ///     Handle dropdown changes
        /// </summary>
        /// <param name="source"></param>
        /// <param name="change"> dropdown change </param>
        /// <param name="modelName"></param>
        public virtual void DropdownValueChanged(string source, Dropdown change, string modelName)
        {
            switch (source)
            {
                case DropdownEnum.Version:
                    // Start with an empty state
                    ModelManager.ResetState();
                    // Change meshs
                    _currentVersion = _versionOptions[change.value];
                    ModelManager.SetMesh(FileManager.LoadMesh(modelName, _currentVersion));
                    // Version loaded, update state
                    ModelManager.Finish();
                    if (FileManager.IsDamageAnalysisCompleted(_currentVersion) == (int) ModelStateEnum.Finished)
                    {
                        _damageAnalysisCompleted = true;
                        ModelManager.Finish();
                    }
                    else
                    {
                        _damageAnalysisCompleted = false;
                    }

                    // Update toggle in UI
                    GameObject.Find("AnalysisCompletedToggle").GetComponent<Toggle>().isOn = _damageAnalysisCompleted;
                    // Enable or disable saving marks on model
                    GameObject.Find("SaveBtn").GetComponent<Button>().interactable = !_damageAnalysisCompleted;
                    // Load texture options
                    FillTextureDropdown();
                    break;
                case DropdownEnum.Texture:
                    // Change texture
                    switch (_textureOptions[change.value])
                    {
                        case DropdownEnum.Original:
                            ModelManager.SetTexture(FileManager.LoadTexture(modelName, _currentVersion, true));
                            break;
                        case DropdownEnum.Modified:
                            ModelManager.SetTexture(FileManager.LoadTexture(modelName, _currentVersion, false));
                            break;
                        case DropdownEnum.None:
                            ModelManager.SetTexture(null);
                            break;
                    }
                    break;
            }
        }

        /// <summary>
        ///     Fills version (mesh) dropdown options
        /// </summary>
        /// <param name="modelName"></param>
        public void FillVersionDropdown(string modelName)
        {
            // Set current model name
            _currentModel = modelName;
            ModelManager.SetGameobject(_currentModel);

            _versionOptions = FileManager.GetModelVersions(_currentModel);
            // Last option is not showing for some reason, so add empty option
            _versionOptions.Add("");
            VersionDropdown.ClearOptions();
            VersionDropdown.AddOptions(_versionOptions);
            // Automatically choose latest version - last option because of alphabetical ordering (-2: empty option and index 0 start)
            VersionDropdown.value = _versionOptions.Count - 2;
            // Should not be needed, but DropdownValueChanged is not called automatically for unknown reasons
            DropdownValueChanged(DropdownEnum.Version, VersionDropdown, _currentModel);
        }

        /// <summary>
        ///     Fills texture dropdown options
        /// </summary>
        public virtual void FillTextureDropdown()
        {
            // Show modified texture option if available
            if (FileManager.IsModifiedTextureAvailable(_currentModel, _currentVersion))
            {
                // Last option is not showing for some reason, so add an empty option
                _textureOptions =
                    new List<string> {DropdownEnum.Original, DropdownEnum.Modified, DropdownEnum.None, ""};
                TextureDropdown.ClearOptions();
                TextureDropdown.AddOptions(_textureOptions);
                // Choose modified texture by default
                TextureDropdown.value = 1;
            }
            else
            {
                // Last option is not showing for some reason, so add an empty option
                _textureOptions = new List<string> {DropdownEnum.Original, DropdownEnum.None, ""};
                TextureDropdown.ClearOptions();
                TextureDropdown.AddOptions(_textureOptions);
                // Choose original texture by default
                TextureDropdown.value = 0;
            }

            // Should not be needed, but DropdownValueChanged is not called automatically for unknown reasons
            DropdownValueChanged(DropdownEnum.Texture, TextureDropdown, _currentModel);
        }

        /// <summary>
        ///     Save the modified texture
        /// </summary>
        public virtual void SaveTexture()
        {
            FileManager.SaveToPNG(ModelManager.GetTexture(), _currentModel, _currentVersion);
            // Set current texture to new modified file
            FillTextureDropdown();
        }

        /// <summary>
        ///     Toggles transparancy on and off
        /// </summary>
        public void ToggleTransparancy(bool value)
        {
            ModelManager.EnableTransparancy(value);
        }

        /// <summary>
        ///     Resets the position and size of the model
        /// </summary>
        public void ResetPos()
        {
            ModelManager.ResetPos();
        }

        /// <summary>
        ///     Toggle the option to scale the model
        /// </summary>
        public void ToggleMarking()
        {
            var active = ModelManager.ToggleMarking();
            // Update button color and image
            GameObject.Find("MarkBtn").GetComponent<Image>().color = active ? Color.green : Color.red;
        }

        /// <summary>
        ///     Toggle the option to scale the model
        /// </summary>
        public void ToggleScaling()
        {
            var active = ModelManager.ToggleScaleScript();
            // Update button color and image
            GameObject.Find("ScaleBtn").GetComponent<Image>().sprite = active
                ? Resources.Load<Sprite>("Images/MagnifierActive")
                : Resources.Load<Sprite>("Images/MagnifierInactive");
            GameObject.Find("ScaleBtn").GetComponent<Image>().color = active ? Color.green : Color.red;

            // RotateBtn and ScaleBtn can't be active at the same time
            if (active) GameObject.Find("RotateBtn").GetComponent<Image>().color = Color.red;
        }

        /// <summary>
        ///     Toggle the option to rotate the model
        /// </summary>
        public void ToggleRotating()
        {
            var active = ModelManager.ToggleRotateScript();
            // Update button color
            GameObject.Find("RotateBtn").GetComponent<Image>().color = active ? Color.green : Color.red;

            // RotateBtn and ScaleBtn can't be active at the same time
            if (active) GameObject.Find("ScaleBtn").GetComponent<Image>().color = Color.red;
        }

        /// <summary>
        ///     Lock 3D-model movement
        /// </summary>
        public void LockMovement()
        {
            var active = ModelManager.ToggleMovementScript();
            // Update button color and image
            GameObject.Find("LockBtn").GetComponent<Image>().sprite = active
                ? Resources.Load<Sprite>("Images/LockOpen")
                : Resources.Load<Sprite>("Images/LockClosed");
            GameObject.Find("LockBtn").GetComponent<Image>().color = active ? Color.green : Color.red;
        }

        /// <summary>
        ///     Toggle damage analysis, restrictions can be applied in the future to enable damage analysis
        /// </summary>
        public virtual void ToggleDamageAnalysisCompletion(bool value)
        {
            // Ignore initial value change toggle is manually updated
            if (_damageAnalysisCompleted != value)
            {
                // Value = damage analysis completed
                var state = value ? ModelStateEnum.Finished : ModelStateEnum.Start;
                // Update XML file with new value
                FileManager.SetDamageAnalysisCompletion(_currentVersion, state);
                _damageAnalysisCompleted = value;
                // Enable or disable saving marks on model
                GameObject.Find("SaveBtn").GetComponent<Button>().interactable = !_damageAnalysisCompleted;
                if (_damageAnalysisCompleted)
                    ModelManager.Finish();
                else
                    ModelManager.ResetToPreviousState();
            }
        }
    }
}