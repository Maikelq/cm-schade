﻿namespace Assets.Scripts
{
    public static class DropdownEnum
    {
        public const string
            // Dropdowns
            Version = "Version",
            Texture = "Texture",
            // Dropdown options
            Original = "Original",
            Modified = "Modified",
            None = "None";
    }
}