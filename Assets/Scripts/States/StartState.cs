﻿using System;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using Color = UnityEngine.Color;

namespace Assets.Scripts.States
{
    public class StartState : IModelState
    {
        public ModelStateEnum PreviousState;

        public StartState(ModelManager modelManager)
        {
            ModelManager = modelManager;
        }

        public ModelManager ModelManager { get; set; }

        /// <summary>
        /// Returns state type as Enum
        /// </summary>
        /// <returns></returns>
        public ModelStateEnum GetStateType()
        {
            return ModelStateEnum.Start;
        }
        
        /// <summary>
        /// Sets the previous state type as Enum
        /// </summary>
        /// <param name="modelStateEnum"></param>
        public void SetPreviousStateType(ModelStateEnum modelStateEnum)
        {
            PreviousState = modelStateEnum;
        }

        /// <summary>
        /// Gets the previous state type as Enum
        /// </summary>
        /// <param name="modelStateEnum"></param>
        public ModelStateEnum GetPreviousStateType()
        {
            return PreviousState;
        }

        /// <summary>
        /// Finish the current state
        /// </summary>
        /// <returns></returns>
        public IModelState Finish()
        {
            IModelState modelState = new FinishedState(ModelManager);
            modelState.SetPreviousStateType(GetStateType());
            return modelState;
        }

        /// <summary>
        /// Apply recent modifications to the texture by changing its color
        /// </summary>
        public virtual void ApplyModification()
        {
            ModelManager.PhaseEnded = true;
            ModelManager.PhaseStarted = false;
            // Color each marked pixel red
            foreach (var p in ModelManager.RecentPixels.Peek()) ModelManager.Texture.SetPixel(p.X, p.Y, Color.red);

            // Apply modified texture - called once per touchcycle as this operation is expensive
            ModelManager.Texture.Apply();
        }

        /// <summary>
        ///     Set rendering mode to transparent
        /// </summary>
        /// <param name="value"></param>
        public virtual void EnableTransparancy(bool value)
        {
            var c = ModelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial.color;
            c.a = value ? 0.5f : 1f;
            ModelManager.ArModel.GetComponent<MeshRenderer>().sharedMaterial.color = c;
        }

        /// <summary>
        ///     Restore previous modifications
        /// </summary>
        public virtual void ReDo()
        {
            // Retrieve modifications made in the latest touchcycle
            foreach (var p in ModelManager.RecentPixels.Peek())
            {
                ModelManager.Texture.SetPixel(p.X, p.Y, ModelManager.UniquePixels[p]);
                ModelManager.UniquePixels.Remove(p);
            }

            ModelManager.Texture.Apply();
            // Remove list
            ModelManager.RecentPixels.Pop();
        }

        /// <summary>
        /// Checks which phase we're in and decides what to do with Input
        /// </summary>
        public void RegisterModelTouch() {
            CheckIfPhaseEnded();

            // No input, wait for next touch cycle
            if (Input.touches.Length == 0)
            {
                ModelManager.TextureApplied = true;
                ModelManager.PhaseStarted = false;
                return;
            }

            var touch = Input.touches[0];
            var ray = Camera.main.ScreenPointToRay(touch.position);

            CheckIfTouchCollidedWithModel(ray);

            // Endphase is sometimes called multiple times - apply modifications once
            if ((Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) &&
                !ModelManager.PhaseEnded)
                ApplyModification();

            // Save current phase to check if cycle has ended properly
            ModelManager.LastPhase = Input.touches[0].phase;
        }

        /// <summary>
        /// Checks if TouchPhase has ended as intended
        /// </summary>
        public virtual void CheckIfPhaseEnded()
        {
            // No input, but phase has not ended correctly due to Unity not working as intended -> manually apply modifications to texture
            if (Input.touches.Length == 0 && ModelManager.LastPhase != TouchPhase.Ended &&
             ModelManager.LastPhase != TouchPhase.Canceled)
            {
                ModelManager.LastPhase = TouchPhase.Ended;
                ApplyModification();
            }
        }

        /// <summary>
        /// Checks if user touch has collided with the 3D-model
        /// </summary>
        /// <param name="ray"></param>
        public void CheckIfTouchCollidedWithModel(Ray ray)
        {
            if (Physics.Raycast(ray, out var hit))
                if (hit.collider.CompareTag("AR-model"))
                {
                    CheckIfBeginPhaseHasStarted();

                    // Retrieve texture and translate 3D to 2D coordinates
                    var renderer = hit.collider.GetComponent<MeshRenderer>();
                    ModelManager.Texture = (Texture2D)renderer.sharedMaterial.mainTexture;

                    var pCoord = hit.textureCoord;
                    pCoord.x *= ModelManager.Texture.width;
                    pCoord.y *= ModelManager.Texture.height;

                    RegisterMarkCoordinates(pCoord);
                }
        }

        /// <summary>
        /// Checks if begin phase has started
        /// </summary>
        public void CheckIfBeginPhaseHasStarted()
        {
            // Begin phase is called multiple times, only respond to first call
            if ((ModelManager.TextureApplied || Input.touches[0].phase == TouchPhase.Began ) &&
                !ModelManager.PhaseStarted)
            {
                ModelManager.TextureApplied = false;
                ModelManager.PhaseStarted = true;
                ModelManager.PhaseEnded = false;
                // Create new List for each touchcycle, so user can undo one cycle at a time
                ModelManager.RecentPixels.Push(new List<ModelManager.Point>());
            }
        }

        /// <summary>
        /// Registers the coordinates of the position touched by the user (and has collided with the 3D-model)
        /// </summary>
        /// <param name="pCoord"></param>
        public void RegisterMarkCoordinates(Vector2 pCoord)
        {
            // Apply modifications in a square around the touched pixel; 1 pixel is too small
            for (var x = -4; x < 4; x++)
            {
                for (var y = -2; y < 2; y++)
                {
                    var markCoord =
                        new ModelManager.Point((int)pCoord.x + x, (int)pCoord.y + y);
                    // Only add new pixels to the list - no point modifying a pixel multiple times
                    if (!ModelManager.UniquePixels.ContainsKey(markCoord))
                    {
                       // Add pixel modification to the latest List (for undoing purposes)
                        ModelManager.RecentPixels.Peek().Add(markCoord);
                        ModelManager.UniquePixels.Add(markCoord,
                            ModelManager.Texture.GetPixel(markCoord.X, markCoord.Y));
                    }
                }
            }
        }

        /// <summary>
        ///     Resets the position and size of the model
        /// </summary>
        public virtual void ResetPos()
        {
            // Original model is too big - default is 0,5
            ModelManager.ArModel.transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
            ModelManager.ArModel.transform.localRotation = Quaternion.identity;
            // To the right of the physical object
            ModelManager.ArModel.transform.localPosition = new Vector3(0.2f, 0, 0.2f);

            // Quick temp fix for Baron model issues
            if (ModelManager.ArModel.transform.name == "Baron")
            {
                ModelManager.ArModel.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                ModelManager.ArModel.transform.localRotation = Quaternion.identity;
                ModelManager.ArModel.transform.localPosition = new Vector3(0.2f, 0.05f, -0.1f);
            }
        }

        /// <summary>
        ///     Enable and disables the ability to mark the 3D-model's texture
        /// </summary>
        public virtual bool ToggleMarking()
        {
            ModelManager.MarkingEnabled = !ModelManager.MarkingEnabled;
            return ModelManager.MarkingEnabled;
        }

        /// <summary>
        ///     Lock 3D-model movement
        /// </summary>
        /// <returns> Script active </returns>
        public virtual bool ToggleMovementScript()
        {
            var enabled = !ModelManager.ArModel.GetComponent<LeanDragTranslate>().enabled;
            ModelManager.ArModel.GetComponent<LeanDragTranslate>().enabled = enabled;
            return enabled;
        }

        /// <summary>
        ///     Enable and disable rotating script
        /// </summary>
        /// <returns> Rotating script active </returns>
        public virtual bool ToggleRotateScript()
        {
            var enabled = !ModelManager.ArModel.GetComponent<LeanTwistRotate>().enabled;
            ModelManager.ArModel.GetComponent<LeanTwistRotate>().enabled = enabled;

            // Both scripts require 2 finger movement, so only 1 can be active to work properly
            if (enabled) ModelManager.ArModel.GetComponent<LeanPinchScale>().enabled = false;

            return enabled;
        }

        /// <summary>
        ///     Enable and disable scaling script
        /// </summary>
        /// <returns> pinch script active </returns>
        public virtual bool ToggleScaleScript()
        {
            var enabled = !ModelManager.ArModel.GetComponent<LeanPinchScale>().enabled;
            ModelManager.ArModel.GetComponent<LeanPinchScale>().enabled = enabled;

            // Both scripts require 2 finger movement, so only 1 can be active to work properly
            if (enabled) ModelManager.ArModel.GetComponent<LeanTwistRotate>().enabled = false;

            return enabled;
        }
    }
}