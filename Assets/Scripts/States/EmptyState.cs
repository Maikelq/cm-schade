﻿using System;

namespace Assets.Scripts.States
{
    public class EmptyState : IModelState
    {
        public ModelStateEnum PreviousState;

        public EmptyState(ModelManager modelManager)
        {
            ModelManager = modelManager;
        }

        public ModelManager ModelManager { get; set; }

        /// <summary>
        /// Returns state type as Enum
        /// </summary>
        /// <returns></returns>
        public ModelStateEnum GetStateType()
        {
            return ModelStateEnum.Empty;
        }

        /// <summary>
        /// Sets the previous state type as Enum
        /// </summary>
        /// <param name="modelStateEnum"></param>
        public void SetPreviousStateType(ModelStateEnum modelStateEnum)
        {
            PreviousState = modelStateEnum;
        }

        /// <summary>
        /// Gets the previous state type as Enum
        /// </summary>
        /// <param name="modelStateEnum"></param>
        public ModelStateEnum GetPreviousStateType()
        {
            return PreviousState;
        }

        /// <summary>
        /// Finish the current state
        /// </summary>
        /// <returns></returns>
        public IModelState Finish()
        {
            return new StartState(ModelManager);
        }

        public void EnableTransparancy(bool value)
        {
            throw new NotImplementedException();
        }

        public void ReDo()
        {
            throw new NotImplementedException();
        }

        public void RegisterModelTouch()
        {
            throw new NotImplementedException();
        }

        public void ResetPos()
        {
            throw new NotImplementedException();
        }

        public bool ToggleMarking()
        {
            throw new NotImplementedException();
        }

        public bool ToggleMovementScript()
        {
            throw new NotImplementedException();
        }

        public bool ToggleRotateScript()
        {
            throw new NotImplementedException();
        }

        public bool ToggleScaleScript()
        {
            throw new NotImplementedException();
        }
    }
}