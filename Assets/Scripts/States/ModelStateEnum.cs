﻿namespace Assets.Scripts.States
{
    public enum ModelStateEnum
    {
        Empty,
        Start,
        Finished
    }
}