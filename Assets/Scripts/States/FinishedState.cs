﻿using System;
using Lean.Touch;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class FinishedState : IModelState
    {
        public ModelStateEnum PreviousState;

        public FinishedState(ModelManager modelManager)
        {
            ModelManager = modelManager;
        }

        public ModelManager ModelManager { get; set; }

        /// <summary>
        /// Returns state type as Enum
        /// </summary>
        /// <returns></returns>
        public ModelStateEnum GetStateType()
        {
            return ModelStateEnum.Finished;
        }

        /// <summary>
        /// Sets the previous state type as Enum
        /// </summary>
        /// <param name="modelStateEnum"></param>
        public void SetPreviousStateType(ModelStateEnum modelStateEnum)
        {
            PreviousState = modelStateEnum;
        }

        /// <summary>
        /// Gets the previous state type as Enum
        /// </summary>
        /// <param name="modelStateEnum"></param>
        public ModelStateEnum GetPreviousStateType()
        {
            return PreviousState;
        }

        public IModelState Finish()
        {
            throw new NotImplementedException();
        }

        public void EnableTransparancy(bool value)
        {
            throw new NotImplementedException();
        }

        public void ReDo()
        {
            throw new NotImplementedException();
        }

        public void RegisterModelTouch()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Resets the position and size of the model
        /// </summary>
        public void ResetPos()
        {
            // original model is too big - default is 0,5
            ModelManager.ArModel.transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
            ModelManager.ArModel.transform.localRotation = Quaternion.identity;
            // to the right of the physical object
            ModelManager.ArModel.transform.localPosition = new Vector3(0.2f, 0, 0.2f);

            //quick temp fix for Baron model issues
            if (ModelManager.ArModel.transform.name == "Baron")
            {
                ModelManager.ArModel.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                ModelManager.ArModel.transform.localRotation = Quaternion.identity;
                ModelManager.ArModel.transform.localPosition = new Vector3(0.2f, 0.05f, -0.1f);
            }
        }

        public bool ToggleMarking()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Lock 3D-model movement
        /// </summary>
        /// <returns> Script active </returns>
        public bool ToggleMovementScript()
        {
            var enabled = !ModelManager.ArModel.GetComponent<LeanDragTranslate>().enabled;
            ModelManager.ArModel.GetComponent<LeanDragTranslate>().enabled = enabled;
            return enabled;
        }

        /// <summary>
        ///     Enable and disable rotating script
        /// </summary>
        /// <returns> Rotating script active </returns>
        public bool ToggleRotateScript()
        {
            var enabled = !ModelManager.ArModel.GetComponent<LeanTwistRotate>().enabled;
            ModelManager.ArModel.GetComponent<LeanTwistRotate>().enabled = enabled;

            // both scripts require 2 finger movement, so only 1 can be active to work properly
            if (enabled) ModelManager.ArModel.GetComponent<LeanPinchScale>().enabled = false;

            return enabled;
        }

        /// <summary>
        ///     Enable and disable scaling script
        /// </summary>
        /// <returns> pinch script active </returns>
        public bool ToggleScaleScript()
        {
            var enabled = !ModelManager.ArModel.GetComponent<LeanPinchScale>().enabled;
            ModelManager.ArModel.GetComponent<LeanPinchScale>().enabled = enabled;

            // both scripts require 2 finger movement, so only 1 can be active to work properly
            if (enabled) ModelManager.ArModel.GetComponent<LeanTwistRotate>().enabled = false;

            return enabled;
        }
    }
}