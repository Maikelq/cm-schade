﻿namespace Assets.Scripts.States
{
    public interface IModelState
    {
        ModelManager ModelManager { get; set; }

        ModelStateEnum GetStateType();

        void SetPreviousStateType(ModelStateEnum modelStateEnum);

        ModelStateEnum GetPreviousStateType();

        IModelState Finish();

        void ResetPos();

        bool ToggleMarking();

        bool ToggleScaleScript();

        bool ToggleRotateScript();

        bool ToggleMovementScript();

        void RegisterModelTouch();

        void ReDo();

        void EnableTransparancy(bool value);
    }
}